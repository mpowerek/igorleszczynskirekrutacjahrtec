IgorLeszczyńskiRekrutacjaHRtec
================
[![License](http://img.shields.io/badge/license-MIT-lightgrey.svg)](https://github.com/SammyK/package-skeleton/blob/master/LICENSE)

Description
------------

- [Installation](#installation)
- [Usage](#usage)
- [Sample Ussage](#sample-ussage)
- [License](#license)


## **Installation**


git clone

## **Ussage**

In the folder, execute the command
``` cli
php src/console.php csv:[simple/extended] [URL] [PATH] [name].csv [HEADERS + options: -strip, -date]
```

Parameters:

``simple`` overwrites the data file

``extended`` adds new data to the current file

Suffix:

suffix ``--strip`` removes html tags

suffix ```--date``` means the variable as a date

## **Sample Ussage**

``` cli
php src/console.php csv:simple http://feeds.nationalgeographic.com/ng/News/News_Main C:\Users\Igor\Desktop\ simple_export.csv title description--strip pubDate--date link creator
```

Credits
-------

- [Igor Leszczyński](https://bitbucket.org/mpowerek/)

## **License**


The MIT License (MIT). Please see [License File](#) for more information.
