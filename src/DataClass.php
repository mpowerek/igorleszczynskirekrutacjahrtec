<?php namespace IgorLeszczynskiRekrutacjaHRtec;
use Exception;
use SimpleXmlElement;

class DataClass
{
    private $url;
    private $path;
    private $csvFilename;
    private $headers;

    /**
     * DataClass constructor.
     * @param string $url
     * @param string $path
     * @param string $filename
     * @param array $headers
     */
    public function __construct($url, $path, $filename, $headers)
    {
        $this->url = $url;
        $this->path = $path;
        $this->csvFilename = $filename;
        $this->headers = $headers;
    }

    /**
     * That gets data from RSS link
     * @return SimpleXmlElement
     */
    public function getRssData()
    {
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return new SimpleXmlElement($data, LIBXML_NOCDATA);
    }

    /**
     * That puts data in CSV file
     * @param $xml
     * @param $mode 0 = overwriting old data, 1 add old data to file
     * @return array Data
     */
    public function parseRSS($xml, $mode)
    {
        $oldData = $this->getOldDataFromCSV();
        if (($output = fopen($this->path . "\\" . $this->csvFilename, 'w')) !== FALSE) {
            $months = ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"];
            $headers = [];
            try {
                foreach ($this->headers as $header)
                    array_push($headers, $header[0]);
                fputcsv($output, $headers);
                foreach ($xml->channel->item as $item) {
                    $test = $this->simpleXmlToArray($item);
                    if(!array_key_exists($header[0], $test))
                        throw new Exception("Header: $header[0] dont exists in data.", 1);
                    $special_items = [];
                    foreach ($this->headers as $header) {
                        foreach ($test as $key => $val) {
                            if ($key == $header[0]) {
                                if ($header[1] == 1)
                                    $special_items[$key] = strip_tags($val);
                                else if ($header[1] == 2) {
                                    $month = $months[date('n', strtotime($val)) - 1];
                                    $day = date('d', strtotime($val));
                                    $rest = date('Y H:i:s', strtotime($val));
                                    $special_items[$key] = $day . ' ' . $month . ' ' . $rest;
                                } else if ($header[1] == 3) {

                                } else if ($header[1] == 0)
                                    $special_items[$key] = $val;
                                else
                                    throw new Exception("One of the headers expects a valid argument\n", 2);
                            }
                        }
                    }
                    fputcsv($output, $special_items);
                }
                if ($mode == 1)
                {
                    foreach ($oldData as $test)
                        fputcsv($output, $test);
                }
            } catch (Exception $e) {
                echo 'Uncaught exception #' . $e->getCode() . ': ' . $e->getMessage();
            }
        }
    }

    /**
     *  It returns old data in file
     * @return array
     */
    private function getOldDataFromCSV()
    {
        if (($handle = fopen($this->path . "\\" . $this->csvFilename, 'r')) !== FALSE) {
            $oldfile = [];
            while (($data = fgetcsv($handle)) !== FALSE) {
                array_push($oldfile, $data);
            }
            fclose($handle);
            return $oldfile;
        }
    }

    /**
     * It changes xmlObject to array
     * @param $xmlObject
     * @return array
     */
    private function simpleXmlToArray($xmlObject)
    {
        $array = [];
        if (!empty($xmlObject)) {
            foreach ($xmlObject->children() as $node) {
                $array[$node->getName()] = is_array($node) ? simplexml_to_array($node) : (string)$node;
                foreach ($xmlObject->children('http://purl.org/dc/elements/1.1/') as $child) {
                    $array[$child->getName()] = is_array($child) ? simplexml_to_array($child) : (string)$child;
                }
            }
        }
        return $array;
    }
}