<?php

use IgorLeszczynskiRekrutacjaHRtec\DataClass;

require_once __DIR__ . '/../vendor/autoload.php';

if ($argv[1] == null || $argv[2] == null || $argv[3] == null || $argv[4] == null || $argv[5] == null) {
    echo "\n\nMissing argument, use: php src/console.php csv:[simple/extended] [URL] [PATH] [name].csv [HEADERS + options: -strip, -date]\n
    Sample ussage:
    \e[0;31;42mphp src/console.php csv:simple http://feeds.nationalgeographic.com/ng/News/News_Main C:\Users\Igor\Desktop\ simple_export.csv HEADER1-strip HEADER2-date HEADER3 etc.\e[0m";
}
else {
    $titles = [];
    foreach (array_slice($argv, 5) as $title) {
        $args = explode("--", $title);
        $args[1] = $args[1] ?? "default";
        // here are the options of output text
        // 0 = default
        // 1 = remove strip tags
        // 3 = date format
        if ($args[1] == "strip")
            array_push($titles, array($args[0], 1));
        elseif ($args[1] == "date")
            array_push($titles, array($args[0], 2));
        else
            array_push($titles, array($args[0], 0));
    }
    $data = new DataClass($argv[2], $argv[3], $argv[4], $titles);
    $xml = $data->getRssData();
    if($argv[1] == 'csv:simple')
        $data->parseRSS($xml,0);
    else if($argv[1] == 'csv:extended')
        $data->parseRSS($xml,1);
    else
        echo "You have chosen the wrong mode.";
}