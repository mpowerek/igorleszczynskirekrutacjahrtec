<?php

use IgorLeszczynskiRekrutacjaHRtec\DataClass;
use PHPUnit\Framework\TestCase as PHPUnit;


require_once __DIR__ . '/../vendor/autoload.php';

class DataClassTest extends PHPUnit
{

    public function testCanSimpleXmlToArray()
    {
        $data = new DataClass("http://feeds.nationalgeographic.com/ng/News/News_Main", __DIR__, "simple_export.csv", ['title', 'link']);
        $rss = $data->getRssData();
        $this->assertInstanceOf(SimpleXMLElement::class, $rss);
    }
}
